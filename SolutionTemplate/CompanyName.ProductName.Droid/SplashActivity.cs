using Android.App;
using Android.Content.PM;
using Android.OS;

namespace CompanyName.ProductName.Droid
{
	[Activity(Label = "droid-Product Name"
		, MainLauncher = true
		, Icon = "@drawable/icon"
		, Theme = "@style/Theme.Splash"
		, NoHistory = true
		, ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			StartActivity(typeof(MainActivity));
		}
	}
}